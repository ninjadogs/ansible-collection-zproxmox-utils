# ansible-collection-zproxmox-utils

Maketargets and playbooks to lighten the handling of the ansible-collection zproxmox.
Run the script `./utils/update_symlinks.sh` to search for `.zvm` and `.zcompose` files and add symlinks in this directories.
