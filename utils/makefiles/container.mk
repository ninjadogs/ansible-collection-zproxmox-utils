.PHONY: containers-start
containers-start: inventory-show ## Starts containers
	ansible-playbook --tags zcompose_start playbook.yml
	make containers-show

.PHONY: containers-remove
containers-remove: inventory-show ## Removes containers
	ansible-playbook --tags zcompose_remove playbook.yml
	make containers-show

.PHONY: containers-restart
containers-restart: ## Removes and starts the containers
	make containers-remove
	make containers-start

.PHONY: containers-redeploy
containers-redeploy: ## Stopps the container and runs the playbook
	make containers-remove
	make playbook-run
