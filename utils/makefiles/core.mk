.ONESHELL:
SHELL := /bin/bash
.DEFAULT_GOAL := help

# Set the ansible variables
GIT_ROOT := $(shell git rev-parse --show-toplevel)
ACTIVE_INVENTORY_FILE := $(GIT_ROOT)/active_inventory
ACTIVE_INVENTORY := $(shell cat $(ACTIVE_INVENTORY_FILE))
export ANSIBLE_INVENTORY=$(GIT_ROOT)/ansible/inventory/$(shell cat $(ACTIVE_INVENTORY_FILE)).yml
export ANSIBLE_ROLES_PATH=$(GIT_ROOT)/ansible/roles
export ANSIBLE_COLLECTIONS_PATH=$(GIT_ROOT)/ansible/collections

# Define variables for the text colors
color1 = $(shell tput setaf 3)
color2 = $(shell tput setaf 5)
reset = $(shell tput sgr0)

ifdef tags
	ANSIBLE_TAGS := --tags ${tags}
endif

ifdef extras
	ANSIBLE_EXTRAS := --extra-vars ${extras}
endif

# -----------------------------------------------------------------------------
# TARGETS
# -----------------------------------------------------------------------------

.PHONY: help
help: ## Displays all available targets and their descriptions
	@echo "Available targets are:"
	@echo
	@awk 'BEGIN { FS = ":.*##" } \
        /^[a-zA-Z_-]+:.*?##/ { \
          printf "$(color1)%-25s$(reset) $(color2)%s$(reset)\n", $$1, $$2 \
        }' $(MAKEFILE_LIST) | sort

.PHONY: pipenv-activate
pipenv-activate:
	cd $(GIT_ROOT) && pipenv shell

.PHONY: playbook-run
playbook-run: inventory-show ## Runs the ansible playbook playbook.yml. Optional tags can be passed with `tags=<tag>`
	ansible-playbook $(ANSIBLE_TAGS) $(ANSIBLE_EXTRAS) playbook.yml

.PHONY: inventory-show
inventory-show: ## Shows the current path for the ansible inventory
	@echo ""
ifneq ("$(wildcard $(ACTIVE_INVENTORY_FILE))", "")
	echo "$(color1)Active inventory is $(color2)$(ACTIVE_INVENTORY)$(reset)"
	echo "Use $(color2)inventory-* maketargets$(reset) to change the inventory"
ifeq ($(ACTIVE_INVENTORY),prod)
	sleep 2
endif
else
	echo 'test' > $(ACTIVE_INVENTORY_FILE)
	echo "Active inventory is now $(color2)'test'$(reset)"
	echo "Use $(color2)inventory-* maketargets$(reset) to change the inventory"
endif

