.PHONY: inventory-test
inventory-test: ## Sets the current inventory to test
	@echo 'test' > $(ACTIVE_INVENTORY_FILE)
	make inventory-show

.PHONY: inventory-prod
inventory-prod: ## Sets the current inventory to prod
	@echo 'prod' > $(ACTIVE_INVENTORY_FILE)
	make inventory-show
