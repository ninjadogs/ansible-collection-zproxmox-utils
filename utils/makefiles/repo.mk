GIT_ROOT := $(shell git rev-parse --show-toplevel)
ZSYMLINKS_PATH := $(shell find $(GIT_ROOT) -type f -name .zsymlinks | grep -v .git)
SYMLINKS_ORIGIN := $(shell dirname $(ZSYMLINKS_PATH))

.PHONY: repo-update_symlinks
repo-update_symlinks: ## updates symbolic links for makefiles
	$(SYMLINKS_ORIGIN)/update_symlinks.sh
