.PHONY: vault-decrypt
vault-decrypt: ## Decrypts all files with the name vault.yml
	find . -name vault.yml -exec ansible-vault decrypt '{}' \;

.PHONY: vault-encrypt
vault-encrypt: ## Encrypts all files with the name vault.yml
	find . -name vault.yml -exec ansible-vault encrypt '{}' \;
