GIT_ROOT := $(shell git rev-parse --show-toplevel)

.PHONY: vm-create
vm-create: inventory-show ## Creates and provisions the vm
	ansible-playbook --tags zvm_create playbook.yml
	ansible-playbook --tags zprovision playbook.yml

.PHONY: vm-start
vm-start: inventory-show ## Starts the VM
	ansible-playbook --tags zvm_start_vm playbook.yml

.PHONY: vm-stop
vm-stop: inventory-show ## Stops the VM
	ansible-playbook --tags zvm_stop_vm playbook.yml

.PHONY: vm-restart
vm-restart: inventory-show ## Stops all containers, shutdown the vm and start the vm again
	make containers-stop
	make vm-shutdown
	make vm-start

.PHONY: vm-reboot
vm-reboot: inventory-show ## Reboots the VM
	ansible-playbook --tags zvm_reboot_vm playbook.yml

.PHONY: vm-shutdown
vm-shutdown: inventory-show ## Shuts down the VM gracefully
	ansible-playbook --tags zvm_shutdown_vm playbook.yml

.PHONY: vm-remove
vm-remove: inventory-show ## Deletes the VM
	ansible-playbook --tags zvm_remove_vm playbook.yml

.PHONY: vm-recreate
vm-recreate: ## Stops, removes and creates the VM
	@make vm-stop
	@make vm-remove
	@make vm-create

.PHONY: vm-deploy
vm-deploy: ## Deploys the virtual machine
	make playbook-run

.PHONY: vm-redeploy
vm-redeploy: ## Stops, removes, creates the VM and runs the ansible playbook
	make vm-stop
	make vm-remove
	make playbook-run

.PHONY: vm-update
vm-update: ## Updates the promox vm configuration
	make playbook-run tags=zvm_update

.PHONY: vm-provision
vm-provision: ## Install docker, common packes and add nfs shares
	make playbook-run tags=zprovision
