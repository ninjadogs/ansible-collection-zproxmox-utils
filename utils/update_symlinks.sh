#!/bin/bash

# paths
SCRIPTPATH=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
FILES_PATH="${SCRIPTPATH}/symlinks"
GIT_ROOT="$(git rev-parse --show-toplevel)"

# zvm
ZVM_FILE=".zvm"
ZVM_FILES="${FILES_PATH}/zvm"

# zcompose
ZCONTAINER_FILE=".zcompose"
ZCONTAINER_FILES="${FILES_PATH}/zcompose"

# colors
reset=$(tput sgr0)
color1=$(tput setaf 3)
color2=$(tput setaf 5)

# -----------------------------------------------------------------------------

function main(){
    for hosts_dir in "${GIT_ROOT}"/*; do
        create_symlinks_recursivly "${hosts_dir}"
    done
}

function create_symlinks_recursivly(){
    local path=$1; shift

    if [[ -f "${path}/${ZVM_FILE}" ]]; then
        echo ''
        echo "============================================================"
        basename "${path}"
        echo "============================================================"
        create_symlinks "${path}" "${ZVM_FILE}" "${ZVM_FILES}"
    fi

    if [[ -f "${path}/${ZCONTAINER_FILE}" ]]; then
        create_symlinks "${path}" "${ZCONTAINER_FILE}" "${ZCONTAINER_FILES}"
    fi

    for item in "${path}"/*; do
        if [[ -d "${item}" ]]; then
            create_symlinks_recursivly "${item}"
        fi
    done
}

function create_symlinks() {
    local symlink_dest=$1
    local markerfile=$2
    local symlink_src=$3

    # print notification
    local markfile_colored="${color1}${markerfile}${reset}"
    local path_to_extract="${symlink_dest%/*/*/*}"
    local shorten_path="${symlink_dest#$path_to_extract/}"
    local shorten_path="${color1}???/${shorten_path}${reset}"
    echo -e "\n- found markerfile ${markfile_colored} in ${shorten_path}"

    # create symlink for each file in directory
    for item in "${symlink_src}"/*; do
        local basename=''
        basename="$(basename "${item}")"

        echo "  - create symlink for ${color2}${markerfile}/${basename}${reset}"
        ln -sf "${item}" "${symlink_dest}"
    done

    # create symlink for ansible.cfg
    echo "  - create symlink for ${color2}ansible.cfg${reset}"
    ln -sf "${FILES_PATH}/ansible.cfg" "${symlink_dest}"

    # add a description to the markerfile
    description='This file indicates that symbolic links for make and configuration files were created in this directory.'
    echo "${description}" > "${symlink_dest}/${markerfile}"
}

# -----------------------------------------------------------------------------

main

